<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInfokajiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infokajians', function (Blueprint $table) {
            $table->id();
            $table->string('judul');
            $table->string('pemateri');
            $table->string('tempat');
            $table->string('alamat');
            $table->string('mulai');
            $table->string('selesai');
            $table->tinyInteger('status');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infokajians');
    }
}
