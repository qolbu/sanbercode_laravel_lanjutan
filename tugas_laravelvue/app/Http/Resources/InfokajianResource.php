<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class InfokajianResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'judul' => $this->judul,
            'pemateri' => $this->pemateri,
            'tempat' => $this->tempat,
            'alamat' => $this->alamat,
            'mulai' => $this->mulai,
            'selesai' => $this->selesai,
            'status' => $this->status
        ];
    }
}
