<?php

namespace App\Http\Controllers;

use App\Infokajian;
use Illuminate\Http\Request;
use App\Http\Resources\InfokajianResource;

class InfokajianController extends Controller
{
    public function index() {
        $infokajians = Infokajian::orderBy('created_at', 'desc')->get();

        return InfokajianResource::collection($infokajians);
    }

    public function store(Request $request) {
        $infokajian = Infokajian::create([
            'judul' => $request->judul,
            'pemateri' => $request->pemateri,
            'tempat' => $request->tempat,
            'alamat' => $request->alamat,
            'mulai' => $request->mulai,
            'selesai' => $request->selesai,
            'status' => $request->status
        ]);

        return $infokajian;
    }

    public function delete($id) {
        Infokajian::destroy($id);
        return 'Success';
    }

    
}
