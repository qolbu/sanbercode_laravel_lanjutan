<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Informasi Kajian Sunnah di Bandung</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet">
    <style>
        .completed {
            text-decoration: line-through;
        }
    </style>
</head>
<body>
<div id="app">
    <div class="container">
    <h3>Informasi Kajian di Bandung</h3>
        <div class="row">            
            <form>
            <div class="form-group col-sm">
                <label for="Judul">Judul Kajian</label>
                <input type="text" class="form-control" name="judul" id="judul" v-model="judul">
            </div>
            <div class="form-group col-lg">
                <label for="pemateri">Pemateri</label>
                <input type="text" class="form-control" name="pemateri" id="pemateri" v-model="pemateri">
            </div>
            <div class="form-group col-sm">
                <label for="tempat">Tempat</label>
                <input type="text" class="form-control" name="tempat" id="tempat" v-model="tempat">
            </div>
            <div class="form-group col-sm">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" name="alamat" id="alamat" v-model="alamat"> 
            </div>
            <div class="form-group col-sm">
                <label for="mulai">Mulai</label>
                <input type="text" class="form-control" name="mulai" id="mulai" v-model="mulai">                 
            </div>
            <div class="form-group col-sm">
                <label for="selesai">Selesai</label>
                <input type="text" class="form-control" name="selesai" id="selesai" v-model="selesai">
            </div>
            <button type="button" class="btn btn-primary" @click="addKajian">Submit</button>    
                  
            </form>
        
            <table class="table">
                <thead>
                    <tr>
                    <th scope="col">Judul</th>
                    <th scope="col">Pemateri</th>
                    <th scope="col">Tempat</th>
                    <th scope="col">Alamat</th>
                    <th scope="col">Mulai</th>
                    <th scope="col">Selesai</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(infokajian, index) in infokajians">
                        <th>
                            <span v-bind:class="{'completed' : infokajian.status}">@{{ infokajian.judul}}</span>                            
                        </th>
                        <th>
                            <span v-bind:class="{'completed' : infokajian.status}">@{{ infokajian.pemateri}}</span>                            
                        </th>
                        <th>
                            <span v-bind:class="{'completed' : infokajian.status}">@{{ infokajian.tempat}}</span>                            
                        </th>
                        <th>
                            <span v-bind:class="{'completed' : infokajian.status}">@{{ infokajian.alamat}}</span>                            
                        </th>
                        <th>
                            <span v-bind:class="{'completed' : infokajian.status}">@{{ infokajian.mulai}}</span>                            
                        </th>
                        <th>
                            <span v-bind:class="{'completed' : infokajian.status}">@{{ infokajian.selesai}}</span>                            
                        </th>
                        <th>
                            <button type="button" v-on:click="removeKajian(index)">X</button>
                            <button type="button" v-on:click="toggleComplete(infokajian)">Batal</button>
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- development version, includes helpful console warnings -->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.1"></script>
<!-- JQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
new Vue({
    el: "#app",
    data: {
        judul : "",
        pemateri : "",
        tempat : "",
        alamat : "",
        mulai : "",
        selesai : "",
        infokajians : []
    },
    methods : {
        addKajian : function() {
            let judul = this.judul.trim();
            let pemateri = this.pemateri.trim();
            let tempat = this.tempat.trim();
            let alamat = this.alamat.trim();
            let mulai = this.mulai.trim();
            let selesai = this.selesai.trim();
            if(selesai) {
                // POST /someUrl
                this.$http.post('/api/infokajian', {judul: judul, pemateri: pemateri, tempat: tempat, alamat: alamat, mulai: mulai, selesai : selesai, status : 0}).then(response => {
                    this.infokajians.unshift(
                        {judul: judul, pemateri: pemateri, tempat: tempat, alamat: alamat, mulai: mulai, selesai : selesai, status : 0}
                    )
                });     
            }
        },
        removeKajian : function(index){
            this.infokajians.splice(index, 1)
        },
        toggleComplete : function(infokajian) {
            infokajian.status = !infokajian.status
        }
    },
    mounted : function() {
            this.$http.get('/api/infokajian').then(response => {
            let result = response.body.data;
            this.infokajians = result;
            });
        }
});
</script>
<script>
$( function() {
    // autocomplete untuk memudahkan demo
    var judul = [
        "Al-Ushul Ats-Tsalatsah",
        "Mengenal Allah Lebih Dekat",
        "Kitab Riyadush Shalihin (Bhs. Sunda)",
        "Tafsir Al-Qur'an Al-Karim",
        "Teladan Rasul",
        "TATA QUR'AN (Tahsin Talaqqi Al-Qur'an)",
        "AISAR (Aku Bisa Bahasa Arab)"
        ];
    var pemateri = [
        "Ustadz Abu Haidar as-Sundawy",
        "Ustadz Abu Umar Indra, S.S.",
        "Ustadz Azis Faturokhman, Lc,. M.S.I.",
        "Ustadz Haidar Askarulqohar, Lc.",
        "Ustadz Fandi Kasbara Harahap, S.Sy"
        ];
    var tempat = [
        "Masjid Al-Ukhuwwah Bandung",
        "Masjid Raya Cipaganti",
        "Masjid PUSDAI Bandung",
        "Masjid Umar bin Khattab Selacau",
        "Masjid Al-Furqan Cipaganti"
        ];
    var alamat = [
        "Jl. Diponegoro 63 Bandung",
        "Jl. Wastukencana Bandung",
        "Jl. Raya Cipaganti Bandung",
        "Jl. Selacau Batujajar KBB",
        "Jl. Jurang Bandung"
        ];
    var mulai = [
        "05:30",
        "06:00",
        "06:30",
        "07:00",
        "07:30",
        "08:00",
        "08:30",
        "09:00",
        "09:30",
        "10:00",
        "10:30",
        "11:00",
        "11:30",
        "12:00"
        ];
    var selesai = [
        "05:30",
        "06:00",
        "06:30",
        "07:00",
        "07:30",
        "08:00",
        "08:30",
        "09:00",
        "09:30",
        "10:00",
        "10:30",
        "11:00",
        "11:30",
        "12:00"
        ];

});
</script>
</body>
</html>