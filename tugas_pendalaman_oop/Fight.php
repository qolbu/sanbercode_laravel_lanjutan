<?php

trait Fight {
    public $attachPower;
    public $deffencePower;

    public function serang($lawan) {
	$lawan->diserang($this);
    	return $this->nama . " sedang menyerang " . $lawan->nama;
    }

    public function diserang($musuh) {
	$this->darah = $this->darah - $musuh->attackPower / $this->deffencePower;	    
    	return $this->nama . " sedang diserang";
    }
}
