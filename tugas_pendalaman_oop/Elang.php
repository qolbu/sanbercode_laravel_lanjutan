<?php
class Elang {
	use Hewan, Fight;

	public function __construct() {
		$this->jumlahKaki = 2;
		$this->keahlian = "Terbang tinggi";
		$this->attackPower = 10;
		$this->deffencePower = 5;
	}

	public function getInfoHewan() {
		$hasil = "<p>Info Hewan </p>";
		$hasil .= "Nama : " . $this->nama . "</br>";
		$hasil .= "Jumlah Kaki : " . $this->jumlahKaki . "</br>";
		$hasil .= "Darah :" . $this->darah . "</br>";
		$hasil .= "Keahlian :" . $this->keahlian . "</br>";
		$hasil .= "Attack Power :" . $this->attackPower . "</br>";
		$hasil .= "Deffence Power :" . $this->deffencePower . "</br>";

		return $hasil;
	}
}
