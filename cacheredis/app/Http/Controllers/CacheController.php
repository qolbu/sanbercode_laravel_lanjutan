<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Request;
use App\Belajar_redis;


class CacheController extends Controller
{
    public function index() {
        return view('welcome');
    }
    
    public function withcache() {
        $paragraph = Cache::remember('paragraph', 1, function() {
            return Belajar_redis::all();
        });

        return view('paragraph', compact('paragraph'));
    }

    public function nocache() {        
        $paragraph = Belajar_redis::all();
        return view('nocache', compact('paragraph'));
    }
}
