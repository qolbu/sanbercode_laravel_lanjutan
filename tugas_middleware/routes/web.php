<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::get('/route-1', 'MiddlewareController@route1')->middleware('roles:superadmin');
Route::get('/route-2', 'MiddlewareController@route2')->middleware('roles:admin,superadmin');
Route::get('/route-3', 'MiddlewareController@route3')->middleware('roles:guest,admin,superadmin');
Route::get('/home', 'HomeController@index');

