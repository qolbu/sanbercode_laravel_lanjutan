<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RolesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        $user = Auth::user();

        foreach($roles as $role){
            if ($request->user()->hasRole($role))   {
                return $next($request);
            }
        }
        abort(403);
    }
}
