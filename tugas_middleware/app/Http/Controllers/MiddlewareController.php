<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MiddlewareController extends Controller
{
    public function route1() {
        echo 'Ini Halaman Route1';
    }

    public function route2() {
        echo 'Ini Halaman Route2';
    }

    public function route3() {
        echo 'Ini Halaman Route3';
    }
}
